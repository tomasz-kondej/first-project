/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wyszukiwarka;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author tomas
 */
public class SearchEngine {

    private String word;//word that is being searched
    private String text;// string that is being searched through
    private boolean isCaseSensitive;//whether capital letters matter or not in search
    private int counter;//counts number of appearances
    private Pattern pattern;//pattern type of searched word
    private int[] indexes;// indexes of begin and end apperances in text
    private String commentSuccess = "Liczba wystąpień w tekście";
    private String commentFailure = "Nie występuje w tekście";
    
    public int getCounter() {
        return counter;
    }

    public String getCommentSuccess() {
        return commentSuccess;
    }

    public void setCommentSuccess(String commSuccess) {
        this.commentSuccess = commSuccess;
    }

    public void setCommentFailure(String commFailure) {
        this.commentFailure = commFailure;
    }

    public String getCommentFailure() {
        return commentFailure;
    }

    public SearchEngine() {

    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getWord() {
        return word;
    }

    public void updateData() {

        if ("".equals(word)) {
            counter = 0;
        } else {

            pattern = isCaseSensitive ?
                    Pattern.compile("\\b" + word + "\\b") : 
                    Pattern.compile("\\b" + word + "\\b", Pattern.CASE_INSENSITIVE);

            Matcher matcher = pattern.matcher(text);

            while (matcher.find()) {
                counter++;
            }
        }
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setCaseSensitive(boolean caseSensitive) {
        this.isCaseSensitive = caseSensitive;

    }

    public boolean containsWord() {
        //
        return getCounter() > 0 ? true : false;
    }
    
    //even number - beginning; odd number end
    public int[]getIndexesOfAppearances(){        
        
        if (counter < 1){
            indexes = new int[0];
        } else {
            indexes = new int[counter * 2];
            
            Matcher matcher = pattern.matcher(text);
            boolean hasNext = matcher.find();
            
            int i = 0;
            while (hasNext) {
                indexes[i] = matcher.start();
                indexes[i + 1] = matcher.end();
                i += 2;
                hasNext = matcher.find();
            }           
        }
        
        return indexes;
    }

    @Override
    public String toString() {
        return containsWord()
                ? word + " - " +  commentSuccess +": "+ getCounter()
                : word + commentFailure;
    }

}
